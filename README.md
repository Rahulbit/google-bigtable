# Google Cloud BigTable Hello World

## Requirements

- Node.js v8.0+
- Yarn or npm client

## Installation

```bash
❯ cd google-bigtable/
❯ yarn
```

## Quick start

### Yarn

```bash 
❯ yarn start
✔ Build completed
```

### npm
````bash
❯ npm start
✔ Build completed
````

## Building

`google-bigtable` builds static assets to the `build` directory by default when running `yarn build`.

````bash
❯ yarn build
✔ Building project completed
````


##Todo
 - Enablie linting and fix lintin errors

