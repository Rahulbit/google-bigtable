import BTable from './libs/BTable';

const app = async () => {
  const TABLE_ID = 'Hello-Bigtable';
  const COLUMN_FAMILY_ID = 'cf1';
  const COLUMN_QUALIFIER = 'greeting';
  const INSTANCE_ID = process.env.INSTANCE_ID || 'test';
  const GCLOUD_PROJECT = process.env.GCLOUD_PROJECT || 'my project';

  if (!INSTANCE_ID) {
    throw new Error('Environment variables for INSTANCE_ID must be set!');
  }

  if (!GCLOUD_PROJECT) {
    throw new Error('Environment variables GCLOUD_PROJECT must be set!');
  }


  try {
  	const table = new BTable({
  		INSTANCE_ID,
  		GCLOUD_PROJECT,
  		TABLE_ID,
  		COLUMN_FAMILY_ID,
  		COLUMN_QUALIFIER,
  	});

  	const [tableExists] = await table.exists();
    if (!tableExists) {
      await table.create(options);
    }

    console.log('Write some greetings to the table');
    const greetings = ['Hello World!', 'Hello Bigtable!', 'Hello Node!'];
    await table.insert(greetings);

    const filter = [
      {
        column: {
          cellLimit: 1, // Only retrieve the most recent version of the cell.
        },
      },
    ];

    console.log('Reading a single row by row key');
    const [singeRow] = await table.getRow('greeting0', filter);
    console.log(`\tRead: ${table.getRowValue(singeRow)}`);


    console.log('Reading the entire table');
    const [allRows] = await table.getRows({ filter });
    for (const row of allRows) {
      console.log(`\tRead: ${getRowValue(row)}`);
    }


    console.log('Delete the table');
    await table.delete();
  } catch (err) {
  	console.error('Something went wrong:', err);
  }


  console.log('this is it');
};
export default app;
