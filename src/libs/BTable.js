import bigtable from '@google-cloud/bigtable';

class BTable {
  constructor({
    INSTANCE_ID,
    GCLOUD_PROJECT,
    TABLE_ID,
    COLUMN_FAMILY_ID,
    COLUMN_QUALIFIER,
  }) {
    this.instanceId = INSTANCE_ID;
    this.project = GCLOUD_PROJECT;
    this.columnFamilyId = COLUMN_FAMILY_ID;
    this.columnQualifier = COLUMN_QUALIFIER;
    this.tableId = TABLE_ID;


    this.bigtableClient = bigtable({
      projectId: GCLOUD_PROJECT,
    });
    this.instance = this.bigtableClient.instance(INSTANCE_ID);
    this.table = this.instance.table(TABLE_ID);
  }


  exists() {
    return this.table.exists();
  }

  create() {
    const options = {
      families: [{
        id: this.columnFamilyId,
        rule: {
          versions: 1,
        },
      }],
    };

    return table.create(options);
  }


  insert(dataArr) {
    const rowsToInsert = dataArr.map((greeting, index) => ({
      key: `greeting${index}`,
      data: {
        [this.columnFamilyId]: {
          [this.columnQualifier]: {
            // Setting the timestamp allows the client to perform retries. If
            // server-side time is used, retries may cause multiple cells to
            // be generated.
            timestamp: new Date(),
            value: greeting,
          },
        },
      },
    }));

    return this.table.insert(rowsToInsert);
  }

  getRowValue(row) {
  		return row.data[this.columnFamilyId][this.columnQualifier][0].value;
  }

  getRow(key, filter = []) {
    return this.table.row(key).get({ filter });
  }

  getRows(filter = []) {
    return table.getRows({ filter });
  }

  delete() {
    return table.delete();
  }
}

export default BTable;
